/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usandoswing;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class frame3 extends JFrame{
    JPanel panel = new JPanel();
    private JButton azul, rosa, amarillo, verde;
    public frame3(){
        setTitle("sada");
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        panel.setLayout(new BorderLayout());
        azul = new JButton("Azul");
        Dimension d = new Dimension();
        d.height=40;
        d.width=100;
        azul.setPreferredSize(d);
        azul.addActionListener(new colorAzul());
        verde = new JButton("Verde");
        d.height=40;
        d.width=100;
        verde.setPreferredSize(d);
        verde.addActionListener(new colorVerde());
        amarillo = new JButton("Amarillo");
        d.height=40;
        d.width=100;
        amarillo.setPreferredSize(d);
        amarillo.addActionListener(new colorAmarillo());
        rosa = new JButton("Rosa");
        d.height=40;
        d.width=100;
        rosa.setPreferredSize(d);
        rosa.addActionListener(new colorRosa());
        panel.add(azul,BorderLayout.SOUTH);
        panel.add(verde,BorderLayout.NORTH);
        panel.add(amarillo,BorderLayout.EAST);
        panel.add(rosa,BorderLayout.WEST);
        add(panel);
        panel.setBackground(Color.red);
    }
        class colorAzul implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e){
                panel.setBackground(Color.blue);
            }
        }
        
        class colorVerde implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e){
                panel.setBackground(Color.green);
            }
        }
        
        class colorAmarillo implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e){
                panel.setBackground(Color.yellow);
            }
        }
        
        class colorRosa implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e){
                panel.setBackground(Color.pink);
            }
        }
    
}


